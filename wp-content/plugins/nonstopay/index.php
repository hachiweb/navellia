<?php 
/*
Plugin Name: Nonstopay integration
Description: Nonstopay integration by hachiweb
Plugin URI: https://hachiweb.com/
Author URI: https://hachiweb.com/
Author: HACHIWEB
License: Public Domain
Version: 1.1
*/


function nonstopayapi_templates()
{   
 
    $temps = [];
    $temps['process-payment.php'] = 'Process Payment'; 
    $temps['payment-confirmation.php'] = 'Payment Confirmation'; 
    return $temps;
}

function regsiter_nonstopay_template($page_templates, $theme, $post){
    $templates = nonstopayapi_templates();
    foreach($templates as $key=>$val){
        $page_templates[$key] = $val;
    }
    return $page_templates;
}
add_filter('theme_page_templates','regsiter_nonstopay_template',10,3);


function select_nonstopay_template($template){
    global $post, $wp_query, $wpdb;
    
    $page_temp_slug = get_page_template_slug($post->ID);
    
    $templates = nonstopayapi_templates();
    
    // print_r($templates);
    // exit;
    
    if(isset($templates[$page_temp_slug])){
        $template = plugin_dir_path(__FILE__).'templates/'.$page_temp_slug;   
    }
    return $template;
}
add_filter('template_include','select_nonstopay_template',99);