<?php
/**

 * Template Name: Payment Confirmation

 */
 
// Get POST PARAM
$id = (isset($_POST['id']) && !empty($_POST['id']))?$_POST['id']:0;
$status = (isset($_POST['status']) && !empty($_POST['status']))?$_POST['status']:0;
$devise = (isset($_POST['devise']) && !empty($_POST['devise']))?$_POST['devise']:0;
$amount = (isset($_POST['amount']) && !empty($_POST['amount']))?$_POST['amount']:0;
// Get signature
$signatureKey = 'X-Nonstopay-Signature';
$headers = array();
if (function_exists('apache_response_headers')) {
    $headers = apache_request_headers();
}
else {
    foreach($_SERVER as $key => $value) {
        if (substr($key,0,5) == "HTTP_") {
            $key = str_replace(" ", "-",
            ucwords(strtolower(str_replace('_', ' ',
            substr($key,5)))));
            $headers[$key] = $value;
        }
    }
}
$signature = $headers[$signatureKey];
$DATA_SECURITY_HASH = $headers[$signatureKey];
$hashData = hash_hmac('sha256', json_encode(['id' => $id, 'amount'
=> $amount, 'devise' => $devise, 'status' => $status]),
'B23Lsyuwxwpa20ZsCYAaEQvj47716XdA9fkRbGcB9DtI8EEJIm') ; 

if( $hashData == $signature ){
    global $woocommerce;
   
    $args = array(
        'status'    => 'wc-pending',  
        'customer'  => get_current_user_id(),  
    );
    
    $pending_orders = wc_get_orders( $args );
    
    if( !empty( $pending_orders ) ) {
        foreach($pending_orders as $key=>$val){
            $pending_order_id[$key] = $val->get_id();
        }
    }
    
    $last_pending_order_id = $pending_order_id[0];
    $order = new WC_Order( $last_pending_order_id );
    $order->update_status('completed' );
    // wp_redirect( "https://navellia.nl/my-account/view-order/".$last_pending_order_id, 300 );
    header("location:"."https://navellia.nl/my-account/view-order/".$last_pending_order_id);
    exit();
    print_r("Payment has been made successfully");
}else{
    print_r("Something went wrong");
}