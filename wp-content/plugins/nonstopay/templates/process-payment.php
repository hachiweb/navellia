<?php

/**

 * Template Name: Process Payment

*/
require __DIR__ . '/vendor/autoload.php';

global $woocommerce;
$order_id = (isset($_GET['order_id']) && !empty($_GET['order_id'])) ? $_GET['order_id']: 0;
$email = (isset($_GET['email']) && !empty($_GET['email'])) ? $_GET['email']: 0;

$order = wc_get_order( $order_id ); 
$total = $order->get_total();
$total = number_format( ($total*1.03), 2 );

use Curl\Curl;
$API_KEY = 'B23Lsyuwxwpa20ZsCYAaEQvj47716XdA9fkRbGcB9DtI8EEJIm'; // 
// URL to our redirection-url service. (IP-restricted call)
$url = 'https://usdc.nonstopay.net/api/btn-create-invoice/get/';
/*
* Data to POST
*/
$data = [
'email' => $email,
'amount' => $total,
'devise' => 'USD'
];
$curl = new Curl();
$curl->setHeader('Content-Type', 'application/json');
$curl->setHeader('API-KEY', $API_KEY);
$curl->post($url, $data);
if ($curl->error) {
    echo 'Error: ' . $curl->errorMessage . "\n";
} 
else 
{
    // echo 'Data server received via POST:' . "\n";
    if( $curl->response->errorCode == 0 ){
        // echo 'redirectUrl: ' . $curl->response->redirectUrl . "\n";
        // echo 'html: ' . $curl->response->html . "\n";
        // wp_redirect( $curl->response->redirectUrl, 300 );
        header("location:".$curl->response->redirectUrl);
        exit();
    }
    else{
        // print_r('here');
        echo $curl->response->errorMessage . "\n";
    }
}