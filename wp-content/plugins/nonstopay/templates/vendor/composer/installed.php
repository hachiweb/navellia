<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '30e908df3ed72ca5e664bc40c621ec339b4782a7',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '30e908df3ed72ca5e664bc40c621ec339b4782a7',
            'dev_requirement' => false,
        ),
        'php-curl-class/php-curl-class' => array(
            'pretty_version' => '9.10.0',
            'version' => '9.10.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-curl-class/php-curl-class',
            'aliases' => array(),
            'reference' => 'b6defb4419a8168ab0ea44948a4d18631310d93e',
            'dev_requirement' => false,
        ),
    ),
);
